import os

import network
from network import *
from graphics import *
import Quoridor
import KoriDoor
loged = False


def _show_menu(msg):
    global loged
    os.system('cls')
    win = GraphWin("Result", 200, 250)
    win.setBackground("purple")
    if msg:
        message = Text(Point(90, 10), msg)
        message.draw(win)

    if not loged:

        message = Text(Point(90, 40), "1.Register")
        message.draw(win)
        message = Text(Point(90, 70), "2.Login")
        message.draw(win)
        message = Text(Point(90, 100), "3.Exit")
        message.draw(win)
        inputbox = Entry(Point(90, 130), 2)
        inputbox.draw(win)

    else:
        message = Text(Point(90, 40), "0. PasswordChange")
        message.draw(win)
        message = Text(Point(90, 70), "1. Dummy")
        message.draw(win)
        message = Text(Point(90, 100), "2. Logout")
        message.draw(win)
        message = Text(Point(90, 130), "3. 2 Player")
        message.draw(win)
        message = Text(Point(90, 160), "4. 4 Player")
        message.draw(win)
        message = Text(Point(90, 190), "5. Exit")
        message.draw(win)

        inputbox = Entry(Point(90, 220), 2)
        inputbox.draw(win)

    win.getMouse()
    win.close()
    return inputbox


def _is_valid(cmd):
    if loged:
        return cmd in ['0', '1', '2', '3', '4', '5']
    else:
        return cmd in ['1', '2', '3', '4', '5']


def _run(cmd):
    global loged

    if not loged:
        if cmd == '1':
            tmp = network.register()
            if tmp:
                return 'you are registered'
            else:
                return 'registration failed'

        if cmd == '2':
            loged = network.login()
            if loged:
                return 'you are logged in'
            else:
                return 'logging in failed'

        if cmd == '3':
            exit()
    else:
        if cmd == '0':
            tmp = network.PasswordChange(loged)
            if tmp == False:
                win = GraphWin("tof", 200, 250)
                win.setBackground("purple")
                message = Text(Point(100, 125), "you are not logged in")
                message.draw(win)
                message = Text(Point(100, 175), "click to continue")
                message.draw(win)
                win.getMouse()
                win.close()

            else:
                win = GraphWin("tof", 600, 600)
                win.setBackground("purple")
                message = Text(Point(275, 275), tmp)
                message.draw(win)
                message = Text(Point(275, 325), "click to continue")
                message.draw(win)
                win.getMouse()
                win.close()

            return
        if cmd == '1':
            m = network.dummy(loged)
            if m == False:
                loged = False
                win = GraphWin("tof", 350, 300)
                win.setBackground("purple")
                message = Text(Point(90, 40), "you are not logged in")
                message.draw(win)
                message = Text(Point(90, 70), "click to continue")
                message.draw(win)
                win.getMouse()
                win.close()

            else:
                win = GraphWin("tof", 350, 300)
                win.setBackground("purple")
                message = Text(Point(175, 150), m)
                message.draw(win)
                message = Text(Point(175, 200), "click to continue")
                message.draw(win)
                win.getMouse()
                win.close()

            return

        if cmd == '2':
            if network.logout() == False:
                loged = False
            if not loged:
                return 'you are logged out'
            else:
                return 'logging out failed'
        print(cmd,cmd == '3')
        if cmd == '3':
            Quoridor.main()
        if cmd == '4':
            KoriDoor.main()
        if cmd == '5':
            exit()


def init():
    message = ''
    while True:
        inputbox = _show_menu(message)
        cmd = inputbox.getText()
        print(cmd)
        if _is_valid(cmd):
            message = _run(cmd)
