import os

from requests import post, get, session
import settings
from graphics import *


def login():
    os.system('cls')
    win = GraphWin("Result", 250, 250)
    win.setBackground("purple")
    message = Text(Point(90, 40), "you are going to login ...")
    message.draw(win)
    message = Text(Point(70, 70), "Enter Username: ")
    message.draw(win)
    message = Text(Point(70, 100), "Enter Password: ")
    message.draw(win)
    inputbox = Entry(Point(180, 70), 10)
    inputbox.draw(win)
    inputbox2 = Entry(Point(180, 100), 10)
    inputbox2.draw(win)

    win.getMouse()
    username = inputbox.getText()
    password = inputbox2.getText()

    win.close()

    s = session()
    r = s.post(settings.url['login'], data={'username': username, 'password': password})

    if r.status_code == 201:
        return s
    else:
        return False


def logout():
    try:
        r = get(settings.url['logout'])
    except:
        os.system('cls')
        win = GraphWin("tof", 250, 250)
        win.setBackground("purple")
        message = Text(Point(125, 125), "your server is not running")
        message.draw(win)
        win.getMouse()
        win.close()
        print("your server is not running")
        exit()
        return

    if r.status_code == 201:
        return False
    else:
        return True


def register():
    win = GraphWin("Result", 400, 400)
    win.setBackground("purple")
    message = Text(Point(200, 30), "you are going to register ...")
    message.draw(win)
    message = Text(Point(100, 70), "Enter Username : ")
    message.draw(win)
    Username = Entry(Point(300, 70), 20)
    Username.draw(win)

    message = Text(Point(100, 100), "Enter Password : ")
    message.draw(win)
    Password = Entry(Point(300, 100), 20)
    Password.draw(win)

    message = Text(Point(100, 130), "Enter Firstname : ")
    message.draw(win)
    Firstname = Entry(Point(300, 130), 20)
    Firstname.draw(win)

    message = Text(Point(100, 160), "Enter Lastname : ")
    message.draw(win)
    Lastname = Entry(Point(300, 160), 20)
    Lastname.draw(win)

    message = Text(Point(100, 190), "Enter Email : ")
    message.draw(win)
    Email = Entry(Point(300, 190), 20)
    Email.draw(win)

    win.getMouse()
    username = Username.getText()
    email = Email.getText()
    lastname = Lastname.getText()
    firstname = Firstname.getText()
    password = Password.getText()
    win.close()
    try:
        r = post(settings.url['register'], data={'username': username,
                                                 'password': password,
                                                 'firstname': firstname,
                                                 'lastname': lastname,
                                                 'email': email
                                                 })
    except:
        win = GraphWin("Result", 300, 300)
        win.setBackground("purple")
        message = Text(Point(150, 150), "your server is not running")
        message.draw(win)
        exit()
        return

    if r.status_code == 201:
        return True
    else:
        return False


def dummy(s):
    if s == False:
        return False

    try:
        r = s.get(settings.url['dummy'])
    except:
        win = GraphWin("Result", 300, 300)
        win.setBackground("purple")
        message = Text(Point(150, 150), "your server is not running")
        message.draw(win)
        exit()

    if r.status_code == 201:
        return r.text
    elif r.status_code == 403:
        return False
    else:
        return 'you are not logged'


def PasswordChange(s):
    if s == False:
        return False

    win = GraphWin("tof", 250, 250)
    win.setBackground("purple")
    message = Text(Point(70, 70), "Enter Password: ")
    message.draw(win)
    inputbox = Entry(Point(180, 70), 10)
    inputbox.draw(win)
    win.getMouse()
    win.close()
    password = inputbox.getText()

    try:
        r = s.post(settings.url['PasswordChange'], data={'password': password})
    except:
        os.system('cls')
        win = GraphWin("tof", 250, 250)
        win.setBackground("purple")
        message = Text(Point(125, 125), "your server is not running")
        message.draw(win)
        win.getMouse()
        win.close()
        exit()
        return
    if r.status_code == 201:
        return r.text
    elif r.status_code == 501:
        return 'you are not logged'
    return False
