from django.contrib.auth import authenticate,login as Login,logout as Logout
from django.http import HttpResponse, response
import datetime
from django.contrib.auth.models import User


def dummy(request):
    if not request.user.is_authenticated:
        return HttpResponse("YOU ARE NOT LOGGED", status=403)

    DTime = str(datetime.datetime.now())
    return HttpResponse(DTime, status=201)


def sign_up(request):
    if request.user.is_authenticated:
        return HttpResponse("YOU ARE ALWAYS LOGGED", status=202)

    if request.method == "POST":
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        first_name = request.POST.get('firstname', False)
        last_name = request.POST.get('lastname', False)
        email = request.POST.get('email', False)

        try:
            NUser = User.objects.create_user(username, email, password)
            NUser.last_name = last_name
            NUser.first_name = first_name

            NUser.save()
            return HttpResponse("YOU ARE REGISTERED",status=201)

        except:
            return HttpResponse("YOUR DATA IS NOT VALID",status=500)

    return HttpResponse("YOUR METHOD IS NOT VALID",status=500)


def login(request):
    if request.user.is_authenticated:
        return HttpResponse("YOU ARE ALWAYS LOGGED", status=202)

    if not(request.method == "POST"):
        return HttpResponse("YOUR METHOD IS NOT VALID", status=500)

    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    user = authenticate(request, username=username, password=password)

    if user is not None:
        Login(request, user)
        return HttpResponse("YOUR ARE LOGGED IN", status=201)

    else:
        return HttpResponse("YOUR DATA IS NOT VALID", status=500)


def logout(request):
    Logout(request)
    return HttpResponse("YOUR ARE LOGGED OUT", status=201)


def PasswordChange(request):
    if not request.user.is_authenticated:
        return HttpResponse("YOU ARE NOT LOGGED IN", status=501)
    if request.method == "POST":
        new_password = request.POST.get('password', False)

        user = User.objects.get(username=request.user.username)

        if user is None:
            return HttpResponse("YOUR DATA IS NOT VALID", status=500)

        user.set_password(new_password)
        user.save()
        return HttpResponse("YOUR PASSWORD IS SUCCESSFULLY CHANGED :)", status=201)

    return HttpResponse("YOUR METHOD IS NOT VALID", status=500)